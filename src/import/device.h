/*
 *   Copyright 2017 Sebastian Kügler <sebas@kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HASS_DEVICE_H
#define HASS_DEVICE_H

#include <QObject>
#include <QDateTime>
#include <QString>
#include <QVariant>
#include <QLoggingCategory>

Q_DECLARE_LOGGING_CATEGORY(HASS)

namespace HomeAssistant
{

class Device: public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString id READ id CONSTANT)
    Q_PROPERTY(QString type READ type CONSTANT)
    Q_PROPERTY(QString state READ state WRITE setState NOTIFY stateChanged)

public:
    explicit Device(const QString &type, const QString &id, QObject *parent = nullptr);
    ~Device();

    QString id() const;
    QString type() const;

    QString state() const;
    void setState(const QString &state);

Q_SIGNALS:
    void stateChanged();

private:
    QString m_state;
    QString m_type;
    QString m_id;
};

} // namespace HomeAssistant

#endif

