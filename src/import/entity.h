/*
 *   Copyright 2017 Sebastian Kügler <sebas@kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HASS_ENTITY_H
#define HASS_ENTITY_H

#include <QObject>
#include <QDateTime>
#include <QJsonObject>
#include <QVariant>
#include <QLoggingCategory>

Q_DECLARE_LOGGING_CATEGORY(HASS)

namespace HomeAssistant
{

class EntityState;


class Entity: public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString entityId READ entityId CONSTANT)
    Q_PROPERTY(QString type READ type CONSTANT)
    Q_PROPERTY(QString friendlyName READ friendlyName NOTIFY entityChanged)
    Q_PROPERTY(QString entityState READ entityState NOTIFY entityChanged)
    //Q_PROPERTY(QString state READ state NOTIFY entityChanged)
    Q_PROPERTY(QJsonObject stateObject READ stateObject WRITE setStateObject NOTIFY entityChanged)

public:
    //explicit Entity(const QString &type = QString(), const QString &id = QString(), QObject *parent = nullptr);
    explicit Entity(const QJsonObject &so, QObject *parent = nullptr);
    virtual ~Entity();

    QString id() const;
    QString type() const;
    QString friendlyName() const;
    QString entityId() const;
    QDateTime lastChanged();

    bool isValid() const;

    QString entityState() const;
    void setState(const QString &state);

    QJsonObject attributes() const;
    QJsonObject stateObject() const;
    void setStateObject(const QJsonObject &stateObject);
    void setEventObject(const QJsonObject &eventObject);

    Q_INVOKABLE void switchLight(bool onoff); // FIXME: should move into subclass

Q_SIGNALS:
    void entityChanged() const;
    void triggerService(QJsonObject &serviceObject) const;

private:
    QJsonObject m_stateObject;
};

} // namespace HomeAssistant

Q_DECLARE_METATYPE(HomeAssistant::Entity*)

#endif

