/*   Copyright 2017 Sebastian Kügler <sebas@kde.org>
 *
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "device.h"

#include <KConfigGroup>
#include <KSharedConfig>

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QString>

#include <QDebug>


namespace HomeAssistant
{

Device::Device(const QString &type, const QString &id, QObject *parent)
    : QObject(parent)
    , m_type(type)
    , m_id(id)
{
}

Device::~Device()
{
}

QString Device::type() const
{
    return m_type;
}

QString Device::id() const
{
    return m_id;
}

QString Device::state() const
{
    return m_state;
}

void Device::setState(const QString &state)
{
    if (m_state != state) {
        m_state = state;
        qCDebug(HASS) << "State Object Set :: emitting stateChanged" ;
        emit stateChanged();
    }
}


} // ns
