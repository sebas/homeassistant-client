/*   Copyright 2017 Sebastian Kügler <sebas@kde.org>
 *
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "entitystate.h"
#include "entity.h"

#include <KConfigGroup>
#include <KSharedConfig>

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>

#include <QDebug>



namespace HomeAssistant
{



EntityState::EntityState(QObject *parent)
    : QObject(parent)
{
}

EntityState::~EntityState()
{
}

void EntityState::addRawEvent(const QString& json)
{
    addEvent(QJsonDocument::fromJson(json.toUtf8()));
}

void EntityState::addEvent(const QJsonDocument &jdoc)
{
    Q_ASSERT(jdoc.object().value(QStringLiteral("type")).toString() == QStringLiteral("event"));

    // parse event
    auto vm = jdoc.object().toVariantMap();
    auto _event = vm[QStringLiteral("event")].toMap();
    auto _data = _event[QStringLiteral("data")].toMap();
    auto entity_id = _data[QStringLiteral("entity_id")].toString();

    const auto _fired = _event[QStringLiteral("time_fired")].toString();
    auto fired_at = QDateTime::fromString(_fired, Qt::ISODateWithMs);
    setEntityId(entity_id);
    setLastUpdated(fired_at);
}

QString EntityState::entityId() const
{
    return m_entityId;
}

void EntityState::setEntityId(const QString &entityId)
{
    if (m_entityId != entityId) {
        qDebug() << "Set entityId" << entityId;
        m_entityId = entityId;
    }
}

QDateTime EntityState::lastUpdated()
{
    return m_lastUpdated;
}

void EntityState::setLastUpdated(const QDateTime &dt)
{
    m_lastUpdated = dt;
    emit stateChanged();
}


} // ns
