/*   Copyright 2017 Sebastian Kügler <sebas@kde.org>
 *
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "entity.h"
#include "json_keys.h"

#include "entitystate.h"

#include <KConfigGroup>
#include <KSharedConfig>

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>

#include <QDebug>



namespace HomeAssistant
{

class EntityState;

// Entity::Entity(const QString &type, const QString &id, QObject *parent)
//     : QObject(parent)
// {
//     qDebug() << " WROOOOONG!!!! new Entity:" << entityId();
//
// }

Entity::Entity(const QJsonObject &so = QJsonObject(), QObject *parent)
    : QObject(parent)
    , m_stateObject(so)
{
    qDebug() << "new Entity:" << type() << id() << entityId();

}

Entity::~Entity()
{
}

bool Entity::isValid() const
{
    return (!type().isEmpty() && !id().isEmpty());
}

QString Entity::type() const
{
    return entityId().split(QStringLiteral(".")).at(0);
}

QString Entity::id() const
{
    return entityId().split(QStringLiteral(".")).at(1);
}

QString Entity::entityId() const
{
    return m_stateObject.value(ENTITY_ID_KEY).toString();
}

QString Entity::friendlyName() const
{
    return attributes()[QStringLiteral("friendly_name")].toString();
}

QString Entity::entityState() const
{
    return m_stateObject.value(STATE_KEY).toString();
}

void Entity::setState(const QString& state)
{
    Q_UNUSED(state);
    qWarning() << "Setting state not supported." << entityId();
}

QDateTime Entity::lastChanged()
{
   const auto &lc = m_stateObject.value(QStringLiteral("last_changed")).toString();
   return QDateTime::fromString(lc, Qt::ISODateWithMs);
}


QJsonObject Entity::stateObject() const
{
    return m_stateObject;
}

QJsonObject Entity::attributes() const
{
    return m_stateObject.value(QStringLiteral("attributes")).toObject();
}

void Entity::setStateObject(const QJsonObject &stateObject)
{
    if (m_stateObject != stateObject) {
        m_stateObject = stateObject;
        qCDebug(HASS) << "State Object Set :: emitting changed";

        emit entityChanged();
    }
}

void Entity::setEventObject(const QJsonObject& eventObject)
{
    // We'll be careful here since we don't want to replace data that are in the state object,
    // but not in the event object
    const auto new_state_object = eventObject.value(QStringLiteral("event")).toObject()
                                    .value(QStringLiteral("data")).toObject()
                                        .value(QStringLiteral("new_state")).toObject();

    // update state
    m_stateObject.insert(STATE_KEY, new_state_object.value(STATE_KEY));
    m_stateObject.insert(LAST_CHANGED_KEY, new_state_object.value(LAST_CHANGED_KEY));
    m_stateObject.insert(LAST_UPDATED_KEY, new_state_object.value(LAST_UPDATED_KEY));
    m_stateObject.insert(ATTRIBUTES_KEY, new_state_object.value(ATTRIBUTES_KEY));

    //qDebug() << "state chagned" << entityId() <<  entityState();
    emit entityChanged();

    // update attributes



}

void Entity::switchLight(bool onoff)
{
    /**
    {
        "id": 24,
        "type": "call_service",
        "domain": "light",
        "service": "turn_on",
        // Optional
        "service_data": {
            "entity_id": "light.kitchen"
        }
    }
    */
    QJsonObject obj;
    obj.insert(TYPE_KEY, QStringLiteral("call_service"));
    obj.insert(DOMAIN_KEY, QStringLiteral("light"));
    if (onoff) {
        obj.insert(SERVICE_KEY, QStringLiteral("turn_on"));
        qDebug() << "Switching"  << entityId() << " ON";
    } else {
        obj.insert(SERVICE_KEY, QStringLiteral("turn_off"));
        qDebug() << "Switching"  << entityId() << " OFF";
    }
    QJsonObject data;
    data.insert(ENTITY_ID_KEY, entityId());
    obj.insert(SERVICE_DATA_KEY, data);
    emit triggerService(obj);
}

} // ns
