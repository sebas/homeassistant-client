/*
 *   Copyright 2017 Sebastian Kügler <sebas@kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hasssocket.h"

#include "entity.h"
#include "json_keys.h"

#include <KConfigGroup>
#include <KSharedConfig>

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QSslSocket>
#include <QSslConfiguration>
#include <QLoggingCategory>

#include <QDebug>

Q_LOGGING_CATEGORY(HASS, "hass")

namespace HomeAssistant
{



HassSocket::HassSocket(QObject *parent)
    : QObject(parent)
{
    KSharedConfigPtr config = KSharedConfig::openConfig(QStringLiteral("homeassistantrc"));
    auto cfg = config->group(QStringLiteral("Host"));
    m_password = cfg.readEntry(QStringLiteral("password"));
    m_host = cfg.readEntry(QStringLiteral("host"));
    m_port = cfg.readEntry(QStringLiteral("port"), QStringLiteral("8123"));
    m_useSsl = cfg.readEntry(QStringLiteral("useSsl"), false);
    m_sslCert = cfg.readEntry(QStringLiteral("sslCert"));
}

HassSocket::~HassSocket()
{
}

QString HassSocket::host() const
{
    return m_host;
}

void HassSocket::setHost(const QString &host)
{
    if (m_host != host) {
        qDebug() << "Set host" << host;
        m_host = host;
        emit hostChanged();
    }
}

QString HassSocket::port() const
{
    return m_port;
}

void HassSocket::setPort(const QString &port)
{
    if (m_port != port) {
        qDebug() << "Set port" << port;
        m_port = port;
        emit portChanged();
    }
}

QString HassSocket::sslCert() const
{
    return m_sslCert;
}

void HassSocket::setSslCert(const QString &sslCert)
{
    if (m_sslCert != sslCert) {
        qDebug() << "Set ssl cert" << sslCert;
        m_sslCert = sslCert;
        emit sslCertChanged();
    }
}

bool HassSocket::useSsl() const
{
    return m_useSsl;
}

void HassSocket::setUseSsl(bool useSsl)
{
    if (m_useSsl != useSsl) {
        qDebug() << "Set useSsl" << useSsl;
        m_useSsl = useSsl;
        emit useSslChanged();
    }
}

QString HassSocket::password() const
{
    return m_password;
}

void HassSocket::setPassword(const QString &pass)
{
    if (m_password != pass) {
        m_password = pass;
        emit passwordChanged();
    }
}

void HassSocket::init()
{
    connectSocket();
}

void HassSocket::cancel()
{
    if (m_reply) {
        m_reply->abort();
        m_reply = nullptr;
    }
}

bool HassSocket::loading() const
{
    return m_loading;
}

QString HassSocket::states() const
{
    return m_states;
}

QString HassSocket::error() const
{
    return m_error;
}

void HassSocket::setError(const QString& err)
{
    if (m_error != err) {
        m_error = err;
        emit errorChanged();
    }
}

void HassSocket::saveCredentials()
{
    KSharedConfigPtr config = KSharedConfig::openConfig(QStringLiteral("homeassistantrc"));
    auto cfg = config->group(QStringLiteral("Host"));
    cfg.writeEntry(QStringLiteral("password"), m_password);
    cfg.writeEntry(QStringLiteral("host"), m_host);
    cfg.writeEntry(QStringLiteral("port"), m_port);
    cfg.writeEntry(QStringLiteral("useSsl"), m_useSsl);
    cfg.writeEntry(QStringLiteral("sslCert"), m_sslCert);

    config->sync();
}

void HassSocket::connectSocket()
{
    if (m_useSsl && !m_sslCert.isEmpty()) {
        qCDebug(HASS) << "Using local certificate:" << m_sslCert;
        QList<QSslCertificate> cert = QSslCertificate::fromPath(m_sslCert);

        auto sslconfig = m_webSocket.sslConfiguration();
        sslconfig.setCaCertificates(cert);
        m_webSocket.setSslConfiguration(sslconfig);
    }

    // Build up URL to the websocket service
    QString prot = QStringLiteral("ws://");
    if (m_useSsl) {
        prot = QStringLiteral("wss://");
    }
    QString url = prot + m_host + QLatin1Char(':') + m_port + QStringLiteral("/api/websocket?api_password=") + m_password;
    qDebug() << "WebSocket server:" << prot + m_host + QLatin1Char(':') + m_port + QStringLiteral("/api/websocket?api_password=") + QStringLiteral("XXXXXXXXXX");

    connect(&m_webSocket, &QWebSocket::connected, this, &HassSocket::onConnected);
    connect(&m_webSocket, &QWebSocket::sslErrors, this, &HassSocket::onSslErrors);
    connect(&m_webSocket, &QWebSocket::textMessageReceived, this, &HassSocket::onTextMessageReceived);
    connect(&m_webSocket, &QWebSocket::disconnected, this, &HassSocket::closed);
    m_webSocket.open(QUrl(url));
}

void HassSocket::onConnected()
{
    qDebug() << "Connected!";
}

void HassSocket::onSslErrors(const QList<QSslError> &errors)
{
    Q_UNUSED(errors);
    QSslError first = errors.at(0);
    qCWarning(HASS) << "onSslErrors: " << first.errorString() << endl;
}

void HassSocket::closed()
{
    qDebug() << "Socket connection closed. Reason:" << m_webSocket.closeReason() << m_webSocket.errorString();
    setError(m_webSocket.errorString());
}

void HassSocket::onTextMessageReceived(QString message)
{
    //qDebug() << "==============> OK Message received from server:" << message;
    QJsonDocument jdoc = QJsonDocument::fromJson(message.toUtf8());
    const QString _type = jdoc.object().value(QStringLiteral("type")).toString();
    const int _id = jdoc.object().value(QStringLiteral("id")).toInt();
    const QString _request = m_requests.value(_id);
    if (_type == QStringLiteral("auth_ok")) {
        qDebug() << "Authentication OK!" << _request << _id;
        emit authenticated();
        request(QStringLiteral("get_states"));
        request(QStringLiteral("get_config"));
        request(QStringLiteral("get_services"));
        subscribeStateChanges();
        return;

    } else if (_type == QStringLiteral("result") && _request == QStringLiteral("get_states")) {
        qDebug() << "===> RESULT" << _type << _request << _id;
        QJsonArray res = jdoc.object().value(QStringLiteral("result")).toArray();
        parseStates(res);
        QJsonDocument rdoc;
        rdoc.setArray(res);
        m_states = QString::fromUtf8(rdoc.toJson());
        //qDebug() << "Full JSON:" << m_states;
        //qDebug() << "FULL MSG:" << message;
        emit statesUpdated();
        return;
    } else if (_type == QStringLiteral("result") && _request == QStringLiteral("get_config")) {
        qDebug() << "Config arrived!" << _id << _request;
        m_config = QString::fromUtf8(jdoc.toJson());
//         qDebug() << "Full JSON:" << m_config;
        parseEntities(message);

        return;
    } else if (_type == QStringLiteral("result") && _request == QStringLiteral("get_services")) {
        qDebug() << "Services arrived!" << _id << _request;
        auto srv = QString::fromUtf8(jdoc.toJson());
        qDebug() << "SERVICES Full JSON:" << srv;
        return;
    } else if (_type == QStringLiteral("event") && _request == QStringLiteral("state_changed")) {
        //qDebug() << "===> EVENT" << _type << _request << _id;
        //qDebug() << "       MSG" << QString::fromUtf8(jdoc.toJson());
        const QString entityId = jdoc.object().value(EVENT_KEY).toObject().value(DATA_KEY).toObject().value(ENTITY_ID_KEY).toString();
        Q_ASSERT(!entityId.isEmpty());
        Entity* event_entity = allEntities()[entityId];
        if (event_entity == nullptr) {
            qWarning() << "Event for unknown entity:" << entityId;
            //qDebug() << "       MSG" << QString::fromUtf8(jdoc.toJson());
            return;
        }
        event_entity->setEventObject(jdoc.object());
        //qDebug() << "Updated:" << event_entity->entityId() << " Now" << event_entity->entityState();

        return;
    } else if (_type == QStringLiteral("result") && _request == QStringLiteral("state_changed")) {
        // This is cool, it just says that we're subscribed to state change events
        return;
    }
    qDebug() << "Something unhandled!" << _type << _id << _request << message;
}

int HassSocket::request(const QString &type)
{
    // Request states ...
    /*
    {
        "id": ${id},
        "type": "${type}"
    }
    */

    QJsonObject state_request;
    id++;
    state_request.insert(QStringLiteral("id"), QJsonValue(id));
    state_request.insert(QStringLiteral("type"), QJsonValue(type));

    QJsonDocument req_doc;
    req_doc.setObject(state_request);
    m_webSocket.sendTextMessage(QString::fromUtf8(req_doc.toJson()));
    m_requests.insert(id, type);
    qDebug() << m_requests;
    return id;
}

void HassSocket::sendServiceRequest(QJsonObject &serviceObject)
{
    id++;
    serviceObject.insert(ID_KEY, id);
    QJsonDocument req_doc;
    req_doc.setObject(serviceObject);
    m_webSocket.sendTextMessage(QString::fromUtf8(req_doc.toJson()));
    //m_requests.insert(id, type);
    qDebug() << "Service request sent" << id << req_doc.toJson();
}

void HassSocket::subscribeStateChanges()
{
        // Subscribe to state change events
        /*
        {
            "id": 18,
            "type": "subscribe_events",
            // Optional
            "event_type": "state_changed"
        }
        */

        auto _type = QStringLiteral("state_changed");
        QJsonObject state_change;
        id++;
        state_change.insert(QStringLiteral("id"), QJsonValue(id));
        state_change.insert(QStringLiteral("type"), QJsonValue(QStringLiteral("subscribe_events")));
        state_change.insert(QStringLiteral("event_type"), QJsonValue(_type));
        m_requests.insert(id, _type);

        QJsonDocument sc_doc;
        sc_doc.setObject(state_change);
        m_webSocket.sendTextMessage(QString::fromUtf8(sc_doc.toJson()));
        //qDebug() << "OK Request sent:" << sc_doc.toJson();
}

void HassSocket::parseEntities(const QString& json)
{
    return;
    QJsonDocument jdoc = QJsonDocument::fromJson(json.toUtf8());
    //const QString _type = jdoc.object().value(QStringLiteral("type")).toString();
    QJsonObject _result = jdoc.object().value(QStringLiteral("result")).toObject();
    QJsonArray _components = _result.value(QStringLiteral("components")).toArray();
    qDebug() << "Component:" << _components.count() <<  _components;

    QStringList ignoredComponents({
        QStringLiteral("sun"),
        QStringLiteral("config"),
        QStringLiteral("api"),
        QStringLiteral("updater"),
        QStringLiteral("discovery"),
        QStringLiteral("conversation"),
        QStringLiteral("config.core"),
        QStringLiteral("group"),
        QStringLiteral("logbook"),
        QStringLiteral("weather"),
        QStringLiteral("introduction"),
        QStringLiteral("history"),
        QStringLiteral("media_player"),
        QStringLiteral("frontend"),
        QStringLiteral("http"),
        QStringLiteral("sensor"),
        QStringLiteral("tts"),
        QStringLiteral("config.group"),
        QStringLiteral("recorder"),
        QStringLiteral("websocket_api"),
        QStringLiteral("zone"),
        QStringLiteral("config.automation"),

        QStringLiteral("device_tracker")
    });

    foreach (const QJsonValue &_c, _components) {
        const auto _cs = _c.toString();
        if (!ignoredComponents.contains(_cs)) {
            qDebug() << "Component:" << _c.toString();
        }
    }
}

void HassSocket::parseStates(const QJsonArray& statesArray)
{
    foreach (const QJsonValue &_c, statesArray) {
        const auto _cs = _c.toString();
        const auto o = _c.toObject();
        const auto &full_id = o.value(ENTITY_ID_KEY).toString();
        QStringList _enls = full_id.split(QStringLiteral("."));
        if (_enls.count() == 2) {
            // We're interested in this
            const auto &_type = _enls.at(0);
            auto componentEntity = new Entity(o, this);
            auto entityId = componentEntity->entityId();
            qDebug() << "new entity:" << entityId;
            if (_type == QStringLiteral("light")) {
                //qDebug() << "Got a light!" << _type << entityId;
                m_lights << componentEntity;
                connect(componentEntity, &QObject::destroyed, this, [this, componentEntity]() {
                    m_lights.removeAll(componentEntity);
                    emit lightsChanged();
                });
                emit lightsChanged();
            } else if (_type == QStringLiteral("media_player")) {
                //qDebug() << "Got a media_player!" << _type << entityId;
                m_mediaPlayers << componentEntity;
                connect(componentEntity, &QObject::destroyed, this, [this, componentEntity]() {
                    m_mediaPlayers.removeAll(componentEntity);
                    emit mediaPlayersChanged();
                });
                emit mediaPlayersChanged();
            } else                 if (_type == QStringLiteral("sensor")) {
                //qDebug() << "Got a sensor!" << _type << entityId;
                m_sensors << componentEntity;
                connect(componentEntity, &QObject::destroyed, this, [this, componentEntity]() {
                    m_sensors.removeAll(componentEntity);
                    emit sensorsChanged();
                });
                emit sensorsChanged();
            }

            m_allEntities.insert(entityId, componentEntity);
            connect(componentEntity, &Entity::triggerService, this, &HassSocket::sendServiceRequest);
            connect(componentEntity, &QObject::destroyed, this, [this, entityId]() {
                m_allEntities.remove(entityId);
            });
            if (entityId == QStringLiteral("zone.home")) {
                qDebug() << "BAZINGA";
                emit homeChanged();
            }
        }
    }
    // FIXME: group.all_lights
}

QHash<QString, Entity*> HassSocket::allEntities() const
{
    return m_allEntities;
}

HomeAssistant::Entity* HassSocket::home() const
{
    return m_allEntities[QStringLiteral("zone.home")];
}


QQmlListProperty<Entity> HassSocket::lights()
{
    qDebug() << "Lights:" << m_lights.count();
    return QQmlListProperty<Entity>(this, m_lights);
}

QQmlListProperty<Entity> HassSocket::mediaPlayers()
{
    qDebug() << "MediaPlayers:" << m_mediaPlayers.count();
    return QQmlListProperty<Entity>(this, m_mediaPlayers);
}

QQmlListProperty<Entity> HassSocket::sensors()
{
    qDebug() << "Sensors:" << m_sensors.count();
    return QQmlListProperty<Entity>(this, m_sensors);
}


} // namespace HomeAssistant

