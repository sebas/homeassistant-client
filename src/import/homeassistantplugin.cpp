/*
 *   Copyright 2017 Sebastian Kügler <sebas@kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "homeassistantplugin.h"
#include "hasssocket.h"
#include "entity.h"

#include <QtQml>

HomeAssistantPlugin::HomeAssistantPlugin(QObject *parent)
    : QQmlExtensionPlugin(parent)
{
}

void HomeAssistantPlugin::registerTypes(const char *uri)
{
    using namespace HomeAssistant;

    Q_ASSERT(QLatin1String(uri) == QLatin1String("org.kde.homeassistant"));

    qmlRegisterType<HassSocket> (uri, 1, 0, "HassSocket");
    qmlRegisterUncreatableType<Entity> (uri, 1, 0, "Entity", QStringLiteral("You can't create entities yourself, these are defined by the server."));
}

