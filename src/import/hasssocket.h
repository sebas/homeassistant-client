/*
 *   Copyright 2017 Sebastian Kügler <sebas@kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HASS_WSOCKET_H
#define HASS_WSOCKET_H

#include <QObject>
#include <QSslError>
#include <QWebSocket>
#include <QQmlListProperty>

class QNetworkReply;

namespace HomeAssistant
{

class Entity;

class HassSocket: public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString host READ host WRITE setHost NOTIFY hostChanged)
    Q_PROPERTY(QString port READ port WRITE setPort NOTIFY portChanged)
    Q_PROPERTY(QString password READ password WRITE setPassword NOTIFY passwordChanged)
    Q_PROPERTY(QString sslCert READ sslCert WRITE setSslCert NOTIFY sslCertChanged)
    Q_PROPERTY(bool useSsl READ useSsl WRITE setUseSsl NOTIFY useSslChanged)

    // Caveat: we have to include the namespace here, otherwise metatype registration will fail!
    Q_PROPERTY(HomeAssistant::Entity* home READ home NOTIFY homeChanged)
    Q_PROPERTY(QQmlListProperty<HomeAssistant::Entity> sensors READ sensors NOTIFY sensorsChanged)
    Q_PROPERTY(QQmlListProperty<HomeAssistant::Entity> lights READ lights NOTIFY lightsChanged)
    Q_PROPERTY(QQmlListProperty<HomeAssistant::Entity> mediaPlayers READ mediaPlayers NOTIFY mediaPlayersChanged)

    Q_PROPERTY(bool loading READ loading NOTIFY loadingChanged)
    Q_PROPERTY(QString states READ states NOTIFY statesUpdated)
    Q_PROPERTY(QString error READ error NOTIFY errorChanged)

public:
    explicit HassSocket(QObject *parent = nullptr);
    ~HassSocket();

    QString host() const;
    void setHost(const QString &url);

    QString port() const;
    void setPort(const QString &port);

    QString sslCert() const;
    void setSslCert(const QString &sslCert);

    bool useSsl() const;
    void setUseSsl(bool useSsl);

    QString password() const;
    void setPassword(const QString &pass);

    QString states() const;
    bool loading() const;

    void setError(const QString &err);
    QString error() const;


    QQmlListProperty<Entity> lights();
    QQmlListProperty<Entity> mediaPlayers();
    QQmlListProperty<Entity> sensors();
    QHash<QString, Entity*> allEntities() const;
    HomeAssistant::Entity* home() const;

    Q_INVOKABLE void init();
    Q_INVOKABLE void cancel();
    Q_INVOKABLE void saveCredentials();

Q_SIGNALS:
    void hostChanged() const;
    void portChanged() const;
    void useSslChanged() const;
    void sslCertChanged() const;
    void passwordChanged() const;

    void countChanged() const;
    void loadingChanged() const;
    void statesUpdated() const;
    void errorChanged() const;

    void lightsChanged() const;
    void mediaPlayersChanged() const;
    void sensorsChanged() const;
    void homeChanged() const;

    void authenticated() const;
    void configReceived() const;


public:
    int count() const;
    void parseEntities(const QString &json);
    void parseStates(const QJsonArray &statesArray);

private Q_SLOTS:
    void onConnected();
    void onTextMessageReceived(QString message);
    void onSslErrors(const QList<QSslError> &errors);

    void closed();

    void sendServiceRequest(QJsonObject &serviceObject);

private:
    int request(const QString &type);
    void connectSocket();
    void requestStates();
    void subscribeStateChanges();

    QWebSocket m_webSocket;
    QNetworkReply *m_reply = nullptr;

    QString m_host;
    QString m_password;
    QString m_port;
    QString m_sslCert;
    bool m_useSsl;

    QString m_states;
    QString m_config;
    QString m_error;

    bool m_loading;

    int id = 0;

    QMap<int, QString> m_requests;

    QHash<QString, Entity*> m_allEntities;
    QList<Entity*> m_lights;
    QList<Entity*> m_mediaPlayers;
    QList<Entity*> m_sensors;

};

} // namespace Solid

#endif

