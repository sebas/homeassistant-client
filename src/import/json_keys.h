/*
 *   Copyright 2017 Sebastian Kügler <sebas@kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef JSON_KEYS_H
#define JSON_KEYS_H

namespace HomeAssistant {

const static QString ATTRIBUTES_KEY = QStringLiteral("attributes");
const static QString DATA_KEY = QStringLiteral("data");
const static QString DOMAIN_KEY = QStringLiteral("domain");
const static QString ENTITY_ID_KEY = QStringLiteral("entity_id");
const static QString EVENT_KEY = QStringLiteral("event");
const static QString ID_KEY = QStringLiteral("id");
const static QString LAST_CHANGED_KEY = QStringLiteral("last_changed");
const static QString LAST_UPDATED_KEY = QStringLiteral("last_updated");
const static QString RESULT_KEY = QStringLiteral("result");
const static QString SERVICE_KEY = QStringLiteral("service");
const static QString SERVICE_DATA_KEY = QStringLiteral("service_data");
const static QString STATE_KEY = QStringLiteral("state");
const static QString TYPE_KEY = QStringLiteral("type");

} // ns
#endif
