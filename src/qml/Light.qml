/*
 *   Copyright 2017 Sebastian Kügler <sebas@kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.1

import org.kde.plasma.extras 2.0 as PlasmaExtras
import org.kde.plasma.core 2.0

import org.kde.homeassistant 1.0 as HomeAssistant

Rectangle {


    property bool lightState: entityState == "on"
    property string lightColor: "yellow"
    property string entity: entityState
    width: parent.width
    height: units.gridUnit * 10

    color: lightColor

    Rectangle {
        anchors {
            margins: units.gridUnit
            centerIn: parent
        }
        color: theme.backgroundColor

        ColumnLayout {
            CheckBox {
                text: friendlyName
                checked: lightState
                onCheckedChanged: {
                    print("Switching " + friendlyName + " " + (checked ? "on" : "off"));
                    switchLight(checked)
                }
            }
//             Label {
//                 Layout.fillWidth: true
//                 text: friendlyName
//             }
        }
    }

    onEntityChanged: {

        lightColor = lightState ? "yellow" : "grey"
    }
}
