/*
 *   Copyright 2017 Sebastian Kügler <sebas@kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.1

import org.kde.plasma.extras 2.0 as PlasmaExtras
import org.kde.plasma.core 2.0

import org.kde.homeassistant 1.0 as HomeAssistant

Rectangle {

    property int delegateHeight : delegateWidth * 0.75
    property int delegateWidth : 128
    property int scaleFactor : 1

    property string entity: entityState
    property string cover: "https://home.vizzzion.net:8123" + stateObject.attributes.entity_picture

    width: parent.width
    height: units.gridUnit * 10

    Image {
        id: thumbImage2
        //source: "mediacontrol"
        //source: cover // triggers a crash when creating an https kio slave :(
        height: parent.height * 0.5
        anchors {
            top: parent.top
            //bottom: parent.bottom
            left: parent.left
            right: parent.right
        }
        Rectangle {
            anchors.fill: parent
        }
    }

    Text {
        id: titleText
        anchors {
            top: thumbImage2.bottom
            left: parent.left
            right: parent.right
            margins: 10
        }
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        text: stateObject.attributes.media_artist + " - " + stateObject.attributes.media_title
        renderType: Text.RichText
    }

    RowLayout {
        id: widText
        anchors {
            top: titleText.bottom
            bottom: parent.bottom
            left: parent.left
            right: parent.right
            margins: 10
        }
        Item {
            Layout.fillWidth: true
        }

        IconItem {
            Layout.preferredHeight: units.gridUnit * 3
            Layout.preferredWidth: units.gridUnit * 3
            source: "media-skip-backward"
        }
        IconItem {
            Layout.preferredHeight: units.gridUnit * 3
            Layout.preferredWidth: units.gridUnit * 3
            source: (entityState == "playing") ? "media-playback-pause" : "media-playback-start"
        }
        IconItem {
            Layout.preferredHeight: units.gridUnit * 3
            Layout.preferredWidth: units.gridUnit * 3
            source: "media-skip-forward"
        }
        Item {
            Layout.fillWidth: true
        }
    }

    onEntityChanged: {
        return;
        print("SC: " + mydump(stateObject.attributes));
        print("Artist:" + stateObject.attributes.media_artist);
        print("Song  :" + stateObject.attributes.media_title);
        print("Pic:" + "https://home.vizzzion.net:8123" + stateObject.attributes.entity_picture);
        //print("Pic:" + "https://home.vizzzion.net:8123" + stateObject.attributes.entity_picture);
    }
}
