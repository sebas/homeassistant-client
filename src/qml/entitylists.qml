/*
 *   Copyright 2017 Sebastian Kügler <sebas@kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.1

import org.kde.plasma.extras 2.0 as PlasmaExtras
import org.kde.plasma.core 2.0

import org.kde.homeassistant 1.0 as HomeAssistant


Rectangle {

    width: 1800
    height: 1200
    color: "lightgrey"

    property bool login_visible: true

    function mydump(arr,level) {
        var dumped_text = "";
        if(!level) level = 0;

        var level_padding = "";
        for(var j=0;j<level+1;j++) level_padding += "    ";

        if(typeof(arr) == 'object') {
            for(var item in arr) {
                var value = arr[item];

                if(typeof(value) == 'object') {
                    dumped_text += level_padding + "'" + item + "' ...\n";
                    dumped_text += mydump(value,level+1);
                } else {
                    dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
                }
            }
        } else {
            dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
        }
        return dumped_text;
    }

    function findentity(id, obj) {
        for (var ent in obj) {
            print(mydump(ent));
            if (obj[ent].entity_id == id) {
                print("Found!" + id);
                return obj[ent];
            }
        }
        print("Entity " + id + " not found.");
        return null;
    }

    HomeAssistant.HassSocket {
        id: hassSocket

        Component.onCompleted: {
            if (hassSocket.password != "" && hassSocket.baseurl != "") {
                hassSocket.init();
            }
        }

        onAuthenticated: {
            login_visible = false
        }
        onHomeChanged: {
            hass_name.text = hassSocket.home.friendlyName
        }

    }

    PlasmaExtras.Heading {
        id: hass_name
        text: "Home Assistant"
        anchors {
            left: parent.left
            top: parent.top
            margins: units.gridUnit
        }
    }

    // ======================= Logic above / UI below

    Login {
        id: login

        visible: opacity != 0
        opacity: login_visible ? 1 : 0
        Behavior on opacity { NumberAnimation { duration: 500 }}

        anchors.centerIn: parent

    }
    RowLayout {

        anchors {
            top: hass_name.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        ListView {
            id: lightsList
            visible: opacity != 0
            opacity: login_visible ? 0 : 1
            Behavior on opacity { NumberAnimation { duration: 500 }}

            Layout.preferredWidth: parent.width / 3
            Layout.fillHeight: true

            spacing: 10

            model: hassSocket.lights
            delegate: Light {}
        }

        ListView {
            id: mediaPlayersList
            visible: opacity != 0
            opacity: login_visible ? 0 : 1
            Behavior on opacity { NumberAnimation { duration: 500 }}

            Layout.preferredWidth: parent.width / 3
            Layout.fillHeight: true

            spacing: 10

            model: hassSocket.mediaPlayers
            delegate: MediaPlayerDelegate {}
        }

        ListView {
            id: sensorList
            visible: opacity != 0
            opacity: login_visible ? 0 : 1
            Behavior on opacity { NumberAnimation { duration: 500 }}

            Layout.preferredWidth: parent.width / 3
            Layout.fillHeight: true

            spacing: 10

            model: hassSocket.sensors
            delegate: AirQualitySensor {
            }
        }
    }
    Component.onCompleted: {
    }
}


