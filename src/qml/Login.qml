/*
 *   Copyright 2017 Sebastian Kügler <sebas@kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import QtQuick.Controls 2.0

import org.kde.plasma.extras 2.0 as PlasmaExtras
import org.kde.plasma.core 2.0

import org.kde.homeassistant 1.0 as HomeAssistant

Item {

    width: childrenRect.width
    height: childrenRect.height
    anchors.margins: units.gridUnit

    property alias host: hostfield.text
    property alias port: portfield.text
    property alias password: passtext.text
    property alias error: errorlabel.text

    PlasmaExtras.Heading {
        text: "Login"
        anchors {
            left: hostfield.left
            bottom: hostfield.top
        }
    }

    Label {
        id: hostlabel
        anchors {
            verticalCenter: hostfield.verticalCenter
            right: hostfield.left
        }
        text: "Host:"
    }

    Label {
        id: passlabel
        anchors {
            verticalCenter: passtext.verticalCenter
            right: passtext.left
        }
        text: "Password:"
    }

    TextField {
        id: hostfield
        width: units.gridUnit * 12
        text: hassSocket.host
    }

    TextField {
        id: portfield
        anchors {
            top: hostfield.bottom
            left: hostfield.left
            right: hostfield.right
        }
        width: units.gridUnit * 12
        text: hassSocket.port
    }

    TextField {
        id: passtext
        anchors {
            top: portfield.bottom
            left: hostfield.left
            right: hostfield.right
        }
        text: hassSocket.password
    }


    CheckBox {
        id: sslcheck
        anchors {
            top: passtext.bottom
            left: hostfield.left
        }
        text: "Use SSL"
        checked: hassSocket.useSsl
    }

    CheckBox {
        id: savecheck
        anchors {
            top: sslcheck.bottom
            left: hostfield.left
        }
        text: "Remember"
    }

    Button {
        id: loginbutton
        anchors {
            top: savecheck.top
            left: savecheck.right
            right: hostfield.right
        }
        text: "Log in"
        onClicked: {
            hassSocket.host = login.host
            hassSocket.port = login.port
            hassSocket.password = login.password
            hassSocket.useSsl = sslcheck.checked
            hassSocket.sslCert = "/home/sebas/hass-pub.pem"
            if (savecheck.checked) {
                hassSocket.saveCredentials();
            }
            hassSocket.init();
        }
    }

    Label {
        id: errorlabel
        anchors {
            top: loginbutton.bottom
            left: savecheck.left
            right: loginbutton.right
        }
        text: hassSocket.error
    }


    Component.onCompleted: {

    }
}
