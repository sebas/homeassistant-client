/*
 *   Copyright 2017 Sebastian Kügler <sebas@kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.1

import org.kde.plasma.extras 2.0 as PlasmaExtras
import org.kde.plasma.core 2.0

import org.kde.homeassistant 1.0 as HomeAssistant


Rectangle {

    width: 800
    height: 600
    color: "lightgrey"

    property bool login_visible: true

    property int iaq_ppm: 0
    property string iaq_name: "Air Quality"
    property string states_json: ""

    function mydump(arr,level) {
        var dumped_text = "";
        if(!level) level = 0;

        var level_padding = "";
        for(var j=0;j<level+1;j++) level_padding += "    ";

        if(typeof(arr) == 'object') {
            for(var item in arr) {
                var value = arr[item];

                if(typeof(value) == 'object') {
                    dumped_text += level_padding + "'" + item + "' ...\n";
                    dumped_text += mydump(value,level+1);
                } else {
                    dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
                }
            }
        } else {
            dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
        }
        return dumped_text;
    }

    function findentity(id, obj) {
        for (var ent in obj) {
            print(mydump(ent));
            if (obj[ent].entity_id == id) {
                print("Found!" + id);
                return obj[ent];
            }
        }
        print("Entity " + id + " not found.");
        return null;
    }

    HomeAssistant.HassSocket {
        id: hassSocket

        Component.onCompleted: {
            if (hassSocket.password != "" && hassSocket.baseurl != "") {
                hassSocket.init();
            }
        }

        onAuthenticated: login_visible = false

        onStatesUpdated: {
            states_json = hassSocket.states;
            var d = eval('new Object(' + hassSocket.states + ')');
            //print("HAS Response:" + hassSocket.states );
            //print("HASS Object: " + mydump(d));
//             print("========================================");
//             print(d[d.length-1].attributes.friendly_name);

            var home = findentity("zone.home", d);
            //print(mydump(home));
            //print("HASS Name: " + home.attributes.friendly_name);
            hass_name.text = home.attributes.friendly_name;

            var iaq = findentity("sensor.appliedsensor_iaqstick", d);
            iaq_ppm = iaq.state;
            iaq_name = iaq.attributes.friendly_name;
            //print("IAQ " + iaq_name + " " + iaq_ppm + " << Cool!");
            //print(mydump(iaq));
        }
    }

    PlasmaExtras.Heading {
        id: hass_name
        text: "Home Assistant"
        anchors {
            left: parent.left
            top: parent.top
            margins: units.gridUnit
        }
    }

    // ======================= Logic above / UI below

    MouseArea {

        anchors.fill: parent

        onClicked: {
            print("Click!")
            rest_states.fetch();
        }

    }

    Login {
        id: login

        visible: opacity != 0
        opacity: login_visible ? 1 : 0
        Behavior on opacity { NumberAnimation { duration: 500 }}

        anchors.centerIn: parent

    }

    GridLayout {
        id: mainArea

        visible: opacity != 0
        opacity: login_visible ? 0 : 1
        Behavior on opacity { NumberAnimation { duration: 500 }}

        columns: 2

        anchors {
            top: hass_name.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            margins: units.gridUnit
        }

        Label {
            text: iaq_name + ":"
        }
        Rectangle {
            Layout.fillWidth: true
            Layout.preferredHeight: units.gridUnit * 2
            color: {
                if (iaq_ppm < 1000) {
                    return "green";
                } else if (iaq_ppm < 1500) {
                    return "orange";
                } else if (iaq_ppm < 2000) {
                    return "darkorange";
                }
                return "red";
            }

//             color: "green"
            Text {
                anchors.centerIn: parent
                text: iaq_ppm
            }

        }

        Item {
            Layout.fillHeight: true
            Layout.columnSpan: 2
        }
    }

    Component.onCompleted: {
    }
}


