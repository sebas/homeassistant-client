/*
 *   Copyright 2017 Sebastian Kügler <sebas@kde.org>
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 2.1 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.1

import org.kde.plasma.extras 2.0 as PlasmaExtras
import org.kde.plasma.core 2.0

import org.kde.homeassistant 1.0 as HomeAssistant

Rectangle {

//     property int delegateHeight : delegateWidth * 0.75
//     property int delegateWidth : 128
//     property int scaleFactor : 1
    property string airColor
    property string airText
    property string airAdvice

    property string entity: entityState
//     property string cover: "https://home.vizzzion.net:8123" + stateObject.attributes.entity_picture

    width: parent.width
    height: units.gridUnit * 10

    color: airColor

    Rectangle {
        anchors {
            margins: units.gridUnit
            centerIn: parent
        }
        color: theme.backgroundColor

        ColumnLayout {
            Label {
                Layout.fillWidth: true
                text: i18n("Air quality is %1 (%2).", airText, entityState)
            }
            Label {
                Layout.fillWidth: true
                text: airAdvice
            }
        }
    }

    onEntityChanged: {
        airAdvice = "";
        if (entityState < 1000) {
            airColor = "lightgreen";
            airText = i18n("fair");
        } else if (entityState < 1500) {
            airColor = "orange";
            airText = i18n("mediocre");
        } else {
            airColor = "red";
            airText = i18n("bad");
            airAdvice = i18n("Please open a window!");
        }
        print("Air quality is now: " + entityState);
        return;
    }
}
