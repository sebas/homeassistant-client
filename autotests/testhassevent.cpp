/*************************************************************************************
 *  Copyright 2017 by Sebastian Kügler <sebas@kde.org>                           *
 *                                                                                   *
 *  This library is free software; you can redistribute it and/or                    *
 *  modify it under the terms of the GNU Lesser General Public                       *
 *  License as published by the Free Software Foundation; either                     *
 *  version 2.1 of the License, or (at your option) any later version.               *
 *                                                                                   *
 *  This library is distributed in the hope that it will be useful,                  *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of                   *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU                *
 *  Lesser General Public License for more details.                                  *
 *                                                                                   *
 *  You should have received a copy of the GNU Lesser General Public                 *
 *  License along with this library; if not, write to the Free Software              *
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA       *
 *************************************************************************************/

#include <QCoreApplication>
#include <QtTest>
#include <QObject>
#include <QSignalSpy>

#include <QFile>
#include <QTextStream>
#include <QLoggingCategory>

#include <KSharedConfig>
#include <KConfigGroup>

#include "hasssocket.h"
#include "device.h"
#include "entity.h"
#include "entitystate.h"
#include "json_keys.h"


using namespace HomeAssistant;

class TestHassEvent : public QObject
{
    Q_OBJECT

public:
    explicit TestHassEvent(QObject *parent = nullptr);

private Q_SLOTS:
    void init();
    void cleanup();

    void testConnect();

    void testParseStates();
    void entityFromSensorEvent();
    void updateStateSensor();
    void updateStateMediaPlayer();

private:
    QString readJson(const QString &fname);
};

TestHassEvent::TestHassEvent(QObject *parent)
    : QObject(parent)
{
}

QString TestHassEvent::readJson(const QString& fname)
{
    QFile f(fname);
    if (!f.open(QFile::ReadOnly | QFile::Text)) {
        qCWarning(HASS) << "Could not open file: " << fname;
        return QString();
    }
    QTextStream in(&f);
    auto out = in.readAll();
    return out;
}

void TestHassEvent::init()
{
}

void TestHassEvent::cleanup()
{
}

void TestHassEvent::testConnect()
{
    KSharedConfigPtr config = KSharedConfig::openConfig(QStringLiteral("homeassistantrc"));
    auto cfg = config->group(QStringLiteral("Host"));
    auto password = cfg.readEntry(QStringLiteral("password"));
    auto host = cfg.readEntry(QStringLiteral("host"));
    auto port = cfg.readEntry(QStringLiteral("port"));
    auto useSsl = cfg.readEntry(QStringLiteral("useSsl"), false);
    auto sslCert = cfg.readEntry(QStringLiteral("sslCert"));

    auto socket = new HassSocket(this);

    QCOMPARE(socket->host(), host);
    QCOMPARE(socket->port(), port);
    QCOMPARE(socket->password(), password);
    QCOMPARE(socket->useSsl(), useSsl);
    QCOMPARE(socket->sslCert(), sslCert);

    socket->init();
    // Assume the config has the right values...
    QSignalSpy authSpy(socket, &HassSocket::authenticated);
    QSignalSpy errSpy(socket, &HassSocket::errorChanged);

    QVERIFY(authSpy.wait(5000));
    QCOMPARE(errSpy.count(), 0);

    delete socket;
}

void TestHassEvent::entityFromSensorEvent()
{
    auto sensorevent = readJson(QFINDTESTDATA("data/sensorevent.json"));

    QJsonDocument jdoc = QJsonDocument::fromJson(sensorevent.toUtf8());
    const auto old_state_object = jdoc.object().value(QStringLiteral("event")).toObject()
                                .value(QStringLiteral("data")).toObject()
                                    .value(QStringLiteral("old_state")).toObject();

    const auto new_state_object = jdoc.object().value(QStringLiteral("event")).toObject()
                                .value(QStringLiteral("data")).toObject()
                                    .value(QStringLiteral("new_state")).toObject();

    // Note that we're creating an Entity from an event object, this is just for testing
    // normally, events should go through setEventObject.
    auto old_entity = new Entity(old_state_object, this);
    auto new_entity = new Entity(new_state_object, this);

    QCOMPARE(old_entity->entityId(), QStringLiteral("sensor.appliedsensor_iaqstick"));
    QCOMPARE(old_entity->entityState(), QStringLiteral("770"));
    QCOMPARE(old_entity->friendlyName(), QStringLiteral("AppliedSensor iAQStick"));

    QCOMPARE(old_entity->entityId(), new_entity->entityId());
    QCOMPARE(old_entity->friendlyName(), new_entity->friendlyName());

    QCOMPARE(new_entity->entityState(), QStringLiteral("801"));
    QCOMPARE(new_entity->attributes()[QStringLiteral("unit_of_measurement")].toString(), QStringLiteral("ppm"));

    const auto dt = QDateTime::fromString(QStringLiteral("2017-06-26T14:57:04.361755+00:00"), Qt::ISODateWithMs);
    QCOMPARE(new_entity->lastChanged(), dt);
    QCOMPARE(new_entity->lastChanged().date().year(), 2017);
    QCOMPARE(new_entity->lastChanged().date().month(), 6);
    QCOMPARE(new_entity->lastChanged().date().day(), 26);
    QCOMPARE(new_entity->lastChanged().time().hour(), 14);
    QCOMPARE(new_entity->lastChanged().time().minute(), 57);
    QCOMPARE(new_entity->lastChanged().time().second(), 04);
    QCOMPARE(new_entity->lastChanged().time().msec(), 362);

}

void TestHassEvent::updateStateSensor()
{
    auto sensorevent = readJson(QFINDTESTDATA("data/sensorevent.json"));
    QJsonDocument jdoc = QJsonDocument::fromJson(sensorevent.toUtf8());

    const auto old_state_object = jdoc.object().value(QStringLiteral("event")).toObject()
                                .value(QStringLiteral("data")).toObject()
                                    .value(QStringLiteral("old_state")).toObject();

    // Note that we're creating an Entity from an event object, this is just for testing
    // normally, events should go through setEventObject.
    auto entity = new Entity(old_state_object, this);
    QCOMPARE(entity->entityState(), QStringLiteral("770"));

    auto d1 = entity->lastChanged();

    QSignalSpy changeSpy(entity, &Entity::entityChanged);

    entity->setEventObject(jdoc.object());
    QCOMPARE(changeSpy.count(), 1);
    QCOMPARE(entity->entityState(), QStringLiteral("801"));
    QVERIFY(d1 < entity->lastChanged());
}

void TestHassEvent::testParseStates()
{
    auto config = readJson(QFINDTESTDATA("data/config.json"));
    auto socket = new HassSocket(this);

    socket->parseEntities(config);

    auto fullstatesjson = readJson(QFINDTESTDATA("data/full-states.json"));
    QJsonDocument jdoc = QJsonDocument::fromJson(fullstatesjson.toUtf8());
    QVERIFY(jdoc.isArray());
    auto states = jdoc.array();
    qDebug() << "----- States:" << states.count();

    socket->parseStates(states);


    auto o1 = QJsonObject();
    o1.insert(QStringLiteral("fookey"), QJsonValue(QStringLiteral("blafarghl")));

    auto o2 = QJsonObject();
    o2.insert(QStringLiteral("fookey"), QJsonValue(QStringLiteral("blafarghl")));

    QCOMPARE(o1, o2);

    delete socket;
}

void TestHassEvent::updateStateMediaPlayer()
{
    auto socket = new HassSocket(this);
    auto fullstatesjson = readJson(QFINDTESTDATA("data/full-states.json"));
    QJsonDocument jdoc = QJsonDocument::fromJson(fullstatesjson.toUtf8());
    QVERIFY(jdoc.isArray());
    auto states = jdoc.array();

    socket->parseStates(states);

    // Find woonkamer mediaplayer
    qDebug() << "KEYS:" << socket->allEntities().keys();
    auto mpw = socket->allEntities()[QStringLiteral("media_player.woonkamer")];
    QCOMPARE(mpw->friendlyName(), QStringLiteral("Woonkamer"));

    auto play_event = readJson(QFINDTESTDATA("data/mediaplayer-event-play.json"));
    QJsonDocument eventdoc = QJsonDocument::fromJson(play_event.toUtf8());

    auto d1 = mpw->lastChanged();
    QCOMPARE(mpw->entityState(), QStringLiteral("off"));
    QCOMPARE(mpw->attributes()[QStringLiteral("supported_features")].toInt(), 21437);

    QSignalSpy changeSpy(mpw, &Entity::entityChanged);

    mpw->setEventObject(eventdoc.object());
    QCOMPARE(changeSpy.count(), 1);

    QVERIFY(d1 < mpw->lastChanged());
    QCOMPARE(mpw->entityState(), QStringLiteral("playing"));
    QCOMPARE(mpw->attributes()[QStringLiteral("supported_features")].toInt(), 21437);
    QCOMPARE(mpw->attributes()[QStringLiteral("volume_level")].toInt(), 1);
    QCOMPARE(mpw->attributes()[QStringLiteral("app_name")].toString(), QStringLiteral("Spotify"));

    auto pause_event = readJson(QFINDTESTDATA("data/mediaplayer-event-pause.json"));
    QJsonDocument pauseeventdoc = QJsonDocument::fromJson(pause_event.toUtf8());

    auto d2 = mpw->lastChanged();
    mpw->setEventObject(pauseeventdoc.object());
    QCOMPARE(changeSpy.count(), 2);
    QCOMPARE(mpw->entityState(), QStringLiteral("paused"));

    delete socket;
}


QTEST_GUILESS_MAIN(TestHassEvent)

#include "testhassevent.moc"
